This SSCCE demonstrates the issues with trying to enumerate Jersey 2 resource methods ahead of time. The [jersey2 integration built into dropwizard metrics](http://metrics.dropwizard.io/3.1.0/manual/jersey/#instrumenting-jersey-2-x) tries to find them all at app launch time, but things like subresource locators and Inflectors cannot be discovered then.

This is one of the reasons I went a different route in [my alternate jersey2 / metrics integration](https://bitbucket.org/marshallpierce/jersey2-metrics), where metrics are created lazily.
