package org.mpierce.metrics;

import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

public class ResourceWithoutPathWithSubResource {
    @Timed
    @GET
    @Path("subResource")
    public String get() {
        return "get";
    }
}
