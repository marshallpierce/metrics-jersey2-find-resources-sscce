package org.mpierce.metrics;

import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("resource")
public class BasicResource {
    @Timed
    @GET
    public String get() {
        return "get";
    }
}
