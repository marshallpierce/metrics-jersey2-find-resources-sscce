package org.mpierce.metrics;

import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.Path;

@Path("resourceWithSubResourceLocatorsToSameSubResource")
public class ResourceWithTwoSubresourceLocatorsToSameSubResource {
    @Timed
    @Path("simpleSrl")
    public ResourceWithoutPathWithSubResource getSub() {
        return new ResourceWithoutPathWithSubResource();
    }

    @Timed
    @Path("{pathParamAsSrl}")
    public ResourceWithoutPathWithSubResource getRes() {
        return new ResourceWithoutPathWithSubResource();
    }
}
