package org.mpierce.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.jersey2.InstrumentedResourceMethodApplicationListener;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import java.util.SortedMap;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogManager;
import javax.servlet.Servlet;
import org.eclipse.jetty.server.NetworkConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public final class MetricsJersey2Test {

    private int port;

    @BeforeClass
    public static void classSetUp() {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
    }

    private Server server;

    protected AsyncHttpClient httpClient;
    protected MetricRegistry registry;

    @Before
    public void setUp() throws Exception {
        registry = new MetricRegistry();

        Servlet servlet =
                new ServletContainer(new TestJersey2App(new InstrumentedResourceMethodApplicationListener(registry)));

        server = getServer(servlet);
        server.start();

        NetworkConnector connector = (NetworkConnector) server.getConnectors()[0];
        port = connector.getLocalPort();

        httpClient = new AsyncHttpClient();
    }

    @After
    public void tearDown() throws Exception {
        httpClient.close();

        server.stop();
    }

    @Test
    public void testNormalTimedResource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resource").getStatusCode());

        assertTimer("org.mpierce.metrics.BasicResource.get");
    }

    @Test
    public void testSubResourceLocatorTimedResource() throws ExecutionException, InterruptedException {
        assertEquals(200,
                req("/resourceWithSubResourceLocatorsToSameSubResource/simpleSrl/subResource").getStatusCode());

        assertTimer("org.mpierce.metrics.ResourceWithoutPathWithSubResource.get");
    }

    @Test
    public void testProgrammaticTimedResource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/programmaticResource").getStatusCode());

        assertTimer("org.mpierce.metrics.TestJersey2App$1.apply");
    }

    private void assertTimer(String name) throws InterruptedException {
        SortedMap<String, Timer> timers = registry.getTimers();

        Timer timer = timers.get(name);

        if (timer == null) {
            fail("Could not find <" + name + ">; instead had:\n" +
                    timers.entrySet().stream()
                            .map(e -> e.getKey() + "=" + e.getValue().getCount())
                            .collect(joining("\n")));
        }

        assertEquals(1, timer.getCount());
    }

    private Response req(String path) throws InterruptedException, ExecutionException {
        return httpClient.prepareGet("http://localhost:" + port + path).execute().get();
    }

    private static Server getServer(Servlet servlet) {
        Server server = new Server(0);
        ServletContextHandler servletHandler = new ServletContextHandler();

        servletHandler.addServlet(new ServletHolder(servlet), "/*");

        server.setHandler(servletHandler);

        return server;
    }
}
