package org.mpierce.metrics;

import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;

public class TestJersey2App extends ResourceConfig {
    public TestJersey2App(Object... components) {

        for (Object component : components) {
            register(component);
        }

        register(BasicResource.class);
        register(ResourceWithTwoSubresourceLocatorsToSameSubResource.class);

        final Resource.Builder resourceBuilder = Resource.builder();
        resourceBuilder.path("programmaticResource");

        final ResourceMethod.Builder methodBuilder = resourceBuilder.addMethod("GET");
        // using a lambda makes jersey sad because it can't reflectively access the type
        //noinspection Convert2Lambda
        methodBuilder.produces(MediaType.TEXT_PLAIN_TYPE)
                .handledBy(new Inflector<ContainerRequestContext, Object>() {
                    @Timed
                    @Override
                    public Object apply(ContainerRequestContext containerRequestContext) {
                        return "Such dynamic. Very resource. Wow.";
                    }
                });

        final Resource resource = resourceBuilder.build();
        registerResources(resource);
    }
}
